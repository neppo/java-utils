<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>br.com.neppo.jlibs</groupId>
    <artifactId>utilities</artifactId>
    <version>0.0.3</version>
    <name>Neppo's Java Utilities</name>
    <description>Frequently occurring code extracted into specific libs</description>
    <url>https://bitbucket.org/neppo/java-utils</url>
    <packaging>jar</packaging>

    <properties>
        <java.version>1.8</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jacksonVersion>2.9.8</jacksonVersion>
        <apacheCommonsLangVersion>3.8.1</apacheCommonsLangVersion>
        <loggerVersion>2.11.2</loggerVersion>
        <feignVersion>10.2.0</feignVersion>
    </properties>

    <dependencies>

        <!-- commons -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${apacheCommonsLangVersion}</version>
        </dependency>
        <dependency>
            <groupId>commons-net</groupId>
            <artifactId>commons-net</artifactId>
            <version>3.6</version>
        </dependency>

        <!-- Jackson -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jacksonVersion}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${jacksonVersion}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jacksonVersion}</version>
        </dependency>

        <!-- log -->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>${loggerVersion}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${loggerVersion}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>${loggerVersion}</version>
        </dependency>

        <dependency>
            <groupId>io.github.openfeign</groupId>
            <artifactId>feign-core</artifactId>
            <version>${feignVersion}</version>
        </dependency>

        <dependency>
            <groupId>com.flipkart.zjsonpatch</groupId>
            <artifactId>zjsonpatch</artifactId>
            <version>0.4.11</version>
        </dependency>
    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>nexus-neppo-snapshots</id>
            <url>https://nexus.neppo.com.br/repository/maven-snapshots</url>
        </snapshotRepository>
        <repository>
            <id>nexus-neppo-releases</id>
            <url>https://nexus.neppo.com.br/repository/maven-releases</url>
        </repository>
    </distributionManagement>

    <scm>
        <connection>scm:git:git://bitbucket.org/neppo/java-utils.git</connection>
        <developerConnection>scm:git:ssh://bitbucket.org:neppo/java-utils.git</developerConnection>
        <url>https://bitbucket.org/neppo/java-utils/src</url>
    </scm>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!--<plugin>
                 <groupId>org.apache.maven.plugins</groupId>
                 <artifactId>maven-javadoc-plugin</artifactId>
                 <version>2.9.1</version>
                 <executions>
                     <execution>
                         <id>attach-javadocs</id>
                         <goals>
                             <goal>jar</goal>
                         </goals>
                     </execution>
                 </executions>
             </plugin>

             <plugin>
                 <groupId>org.sonatype.plugins</groupId>
                 <artifactId>nexus-staging-maven-plugin</artifactId>
                 <version>1.6.7</version>
                 <extensions>true</extensions>
                 <configuration>
                     <serverId>ossrh</serverId>
                     <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                     <autoReleaseAfterClose>true</autoReleaseAfterClose>
                 </configuration>
             </plugin>-->
        </plugins>
    </build>

    <developers>
        <developer>
            <name>Braulio Almeida Lima</name>
            <email>braulio.lima@neppo.com.br</email>
            <organization>Neppo</organization>
            <organizationUrl>http://www.neppo.com.br</organizationUrl>
        </developer>

        <developer>
            <name>Antonio da Silva Assunção Filho</name>
            <email>antonio.filho@neppo.com.br</email>
            <organization>Neppo</organization>
            <organizationUrl>http://www.neppo.com.br</organizationUrl>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>The Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        </license>
    </licenses>

    <!--
    <profiles>
        <profile>
            <id>maven-central</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.5</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
            <distributionManagement>
                <snapshotRepository>
                    <id>ossrh</id>
                    <url>https://oss.sonatype.org/content/repositories/snapshots</url>
                </snapshotRepository>
                <repository>
                    <id>ossrh</id>
                    <url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
                </repository>
            </distributionManagement>
        </profile>
    </profiles>-->

</project>