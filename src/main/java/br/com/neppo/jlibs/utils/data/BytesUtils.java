package br.com.neppo.jlibs.utils.data;

public class BytesUtils{

    private BytesUtils(){  }

    public static boolean isEmpty(byte[] arr){
        return arr == null || arr.length == 0;
    }

    public static boolean isNotEmpty(byte[] arr){
        return !isEmpty(arr);
    }

    public static char btoa(byte b){
        return (char) b;
    }

    public static byte atob(char a){
        return (byte) a;
    }

    public static byte[] toByteArray(char[] chars){
        if(chars == null || chars.length == 0){
            return new byte[0];
        }
        byte[] bytes = new byte[chars.length];
        for(int i = 0; i < chars.length; i++){
            bytes[i] = atob(chars[i]);
        }
        return bytes;
    }
    
    public static char[] toCharArray(byte[] bytes){
        if(bytes == null || bytes.length == 0){
            return new char[0];
        }
        char[] chars = new char[bytes.length];
        for(int i = 0; i < bytes.length; i++){
            chars[i] = btoa(bytes[i]);
        }
        return chars;
    }

    public static String toString(char[] c){
        return new String(c);
    }

    public static String toString(byte[] b){
        return toString(toCharArray(b));
    }
}