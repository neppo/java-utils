package br.com.neppo.jlibs.utils.flow.comparison.interfaces;

public interface PreviousCurrent {
    boolean compare(Object previous, Object current);
}
