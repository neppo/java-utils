package br.com.neppo.jlibs.utils.net;

import feign.Response;

public class FeignUtils {

    private FeignUtils() {
    }

    public static boolean isSuccess(Response response){
        if(response == null){
            return false;
        }
        return response.status() >= 200 && response.status() < 300;
    }

}