package br.com.neppo.jlibs.utils.flow.comparison.interfaces;

public interface Current {
    boolean compare(Object a);
}
