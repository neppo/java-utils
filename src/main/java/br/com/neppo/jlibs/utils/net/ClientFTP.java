package br.com.neppo.jlibs.utils.net;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClientFTP implements FileTransfer{

    private final FTPClient ftp = new FTPClient();
    private static final Logger log = LoggerFactory.getLogger(ClientFTP.class);

    @Override
    public boolean connect(String host, int port, String login, String password) {
        try {

            ftp.connect(host, port);
            ftp.login(login, password);
            ftp.enterLocalPassiveMode();

        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return false;
        }

        return ftp.isConnected() && FTPReply.USER_LOGGED_IN == ftp.getReplyCode();
    }

    @Override
    public void disconnect() {
        try {
            ftp.disconnect();
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    @Override
    public List<String> listFilesInDir(String remoteDir) throws Exception {
        return Arrays.stream(ftp.listFiles(remoteDir)).filter(FTPFile::isFile).map(FTPFile::getName).collect(Collectors.toList());
    }

    @Override
    public List<String> listSubDirInDir(String remoteDir) throws Exception {
        ftp.setDataTimeout(5000);
        return Arrays.stream(ftp.listDirectories(remoteDir)).filter(FTPFile::isDirectory).map(FTPFile::getName).collect(Collectors.toList());
    }

    @Override
    public boolean createDirectory(String dirName) {
        try {
            return ftp.makeDirectory(dirName);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        return false;
    }

    @Override
    public boolean downloadFile(String remotePath, String localPath) {
        try (FileOutputStream outputSrr = new FileOutputStream(localPath)) {
            ftp.retrieveFile(remotePath, outputSrr);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    @Override
    public boolean downloadFileAfterCheck(String remotePath, String localPath) throws IOException {
        FileOutputStream outputSrr = null;
        try {

            File file = new File(localPath);
            if (!file.exists()) {
                outputSrr = new FileOutputStream(localPath);
                ftp.retrieveFile(remotePath, outputSrr);
            }

        } catch (IOException e) {

            log.error(e.getMessage(), e);
            return false;

        } finally {
            if (outputSrr != null) {
                outputSrr.close();
            }
        }

        return true;
    }

    @Override
    public boolean uploadFile(String localPath, String remotePath) throws IOException {
        return ftp.storeFile(remotePath, new FileInputStream(localPath));
    }

    @Override
    public boolean uploadFile(InputStream inputFile, String remotePath) throws IOException {
        return ftp.storeFile(remotePath, inputFile);
    }

    @Override
    public boolean changeDir(String remotePath) throws Exception {
        return ftp.changeWorkingDirectory(remotePath);
    }

    @Override
    public String getWorkingDirectory() {
        try {
            return ftp.printWorkingDirectory();
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        return null;
    }

    @Override
    public InputStream downloadFile(String remotePath) throws Exception {
        return ftp.retrieveFileStream(remotePath);
    }

    @Override
    public void moveFilesBetween(String remoteOldpath, String remoteNewpath) throws Exception {
        ftp.rename(remoteOldpath, remoteNewpath);
    }

    @Override
    public boolean fileExists(String remotePath) throws IOException {
        return ftp.retrieveFileStream(remotePath) != null;
    }

}
