package br.com.neppo.jlibs.utils.data.text;

public class StringBuildingUtils {
    private StringBuildingUtils() {
    }

    public static StringBuilder applyByteArray(StringBuilder sb, byte[] arr) {
        if (sb == null) {
            return null;
        }

        if (arr == null || arr.length == 0) {
            return sb;
        }

        for (int i = 0; i < arr.length; i++) {
            sb.append((char) arr[i]);
        }

        return sb;
    }
}