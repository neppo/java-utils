package br.com.neppo.jlibs.utils.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class MapUtils {

    private MapUtils() { }

    private static Object fetchItem(Object object, String[] item, int index){
        if(object == null || item == null || item.length == 0){
            return null;
        }
        if(item.length == index){
            return object;
        }

        if(object instanceof Map){
            return fetchItem(((Map) object).get(item[index]), item, index + 1);
        }

        return null;
    }

    public static Object fetchItem(Object o, String[] item){
        return fetchItem(o, item, 0);
    }

    public static Object fetchItem(Object o, String path){
        return fetchItem(o, path != null ? path.split("\\.") : null);
    }

    public static <T> T deserializeMap(Map<Object, Object> object, Class<T> target){
        if(object == null || target == null){
            return null;
        }
        try{
            return new ObjectMapper().convertValue(object, target);
        }
        catch (Exception e){
            return null;
        }
    }

    @SuppressWarnings({"unchecked"})
    public static <T> Map<String, Object> toMap(T item){
        if(item == null){
            return null;
        }
        try{
            return new ObjectMapper().convertValue(item, Map.class);
        }
        catch (Exception e){
            return null;
        }
    }
}