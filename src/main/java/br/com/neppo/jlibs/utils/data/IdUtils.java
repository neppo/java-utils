package br.com.neppo.jlibs.utils.data;

import br.com.neppo.jlibs.utils.data.reflect.GetSetUtils;
import br.com.neppo.jlibs.utils.flow.EqualityUtils;
import br.com.neppo.jlibs.utils.flow.comparison.ComparisonUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Validates IDs according to the ID_TYPE and GET_ID_METHOD values;
 */
public class IdUtils <T> {

    private static String makeId(String method, Class type){
        if(StringUtils.isEmpty(method) || type == null) {
            return "";
        }
        return String.format("%s::%s", method, type.getName());
    }

    public static <T> IdUtils<T> Make(String idGetter, Class<T> idType){
        String id = makeId(idGetter, idType);
        if(collection.containsKey(id)){
            return collection.get(id);
        }
        IdUtils<T> idUtils = new IdUtils<>(idGetter, idType);
        collection.put(id, idUtils);
        return idUtils;
    }

    private static final Map<String, IdUtils> collection = new HashMap<>();
    public static final IdUtils<String> OfString = Make("getId", String.class);

    private IdUtils(String getter, Class<T> clazz) {
        this.ID_TYPE = clazz;
        this.GET_ID_FIELD = getter;
    }

    private Class<T> ID_TYPE;
    private String GET_ID_FIELD;

    public boolean idEmpty(Object o){
        if(o == null){
            return true;
        }

        Object val = getId(o);

        if(val == null){
            return true;
        }

        return false;
    }

    public T getId(Object o){
        return GetSetUtils.get(o, GET_ID_FIELD, ID_TYPE);
    }

    public <Y> boolean allSame(Collection<Y> objs){
        return ComparisonUtils.applyComparison((a, b) -> {

            Object idA = getId(a);
            Object idB = getId(b);

            return EqualityUtils.equals(idA, idB);

        }, objs);
    }

    public <Y> boolean allUnique(Collection<Y> objs){
        Set<T> s = new HashSet<>();
        return ComparisonUtils.applySingleComparison(a -> {
            T id = getId(a);

            // is unique?
            if(s.contains(id)){
                return false;
            }

            s.add(id);
            return true;
        }, objs);
    }

    public boolean isEmpty(Collection<?> objects){
        if(objects == null){
            objects = new ArrayList<>();
        }
        return ComparisonUtils.applySingleComparison(this::idEmpty, objects);
    }

    public boolean isFull(Collection<?> objects){
        if(objects == null){
            objects = new ArrayList<>();
        }
        return ComparisonUtils.applySingleComparison(item -> !idEmpty(item), objects);
    }

    public List<T> toListOfId(Collection<?> objects){
        if(objects == null){
            objects = new ArrayList<>();
        }
        return objects
                .stream()
                .map(this::getId)
                .collect(Collectors.toList());
    }

    public Set<T> toSetOfId(Collection<?> objects){
        if(objects == null){
            objects = new ArrayList<>();
        }
        return objects
                .stream()
                .map(this::getId)
                .collect(Collectors.toSet());
    }

    /**
     * Makes map based on ID and Entity
     * @param items
     * @param <Y>
     * @return
     */
    public <Y> Map<T, Y> toMap(Collection<Y> items){
        if(items == null){
            items = new ArrayList<>();
        }

        Map<T, Y> map = new HashMap<>();

        items.forEach(item -> {
            if(item == null) return;
            map.put(getId(item), item);
        });

        return map;
    }

}
