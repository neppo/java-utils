package br.com.neppo.jlibs.utils.net;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.neppo.jlibs.utils.data.BytesUtils;
import br.com.neppo.jlibs.utils.data.text.StringBuildingUtils;
import br.com.neppo.jlibs.utils.data.text.TextHelpers;
import br.com.neppo.jlibs.utils.net.pojo.FormField;

public class FormdataUtils {

    private FormdataUtils() { }

    public static final String MULTIPART_BOUNDARY = "--------------48The4JEmpireIHIODFStrikesfdf9Back84jk38fnrJHY8";
    private static final String HTTP_NEW_LINE = "\r\n";
    private static final String HTTP_DOUBLE_DASH = "--";

    public static String buildBodyFromFields(FormField... fields) {
        return buildBodyFromFields(Arrays.asList(fields));
    }

    public static String buildBodyFromFields(List<FormField> fields){

        if(fields == null){
            return "";
        }

        StringBuilder b = new StringBuilder();

        fields.forEach(field -> {
            if(field == null || StringUtils.isEmpty(field.getName()) || BytesUtils.isEmpty(field.getValue())){
                return;
            }

            b.append(HTTP_DOUBLE_DASH).append(MULTIPART_BOUNDARY).append(HTTP_NEW_LINE);

            // field name
            b.append("Content-Disposition: form-data; ").append("name=").append(TextHelpers.wrapQuote(field.getName()));
            if(field.isFile()) b.append("; filename=").append(TextHelpers.wrapQuote(field.getFilename()));
            b.append(HTTP_NEW_LINE);

            // headers
            field.getHeaders().keySet().forEach(key -> b.append(key).append(": ").append(field.getHeaders().get(key)).append(HTTP_NEW_LINE));
            b.append(HTTP_NEW_LINE);

            // field values
            StringBuildingUtils.applyByteArray(b, field.getValue());
            b.append(HTTP_NEW_LINE);
        });
        b.append(HTTP_DOUBLE_DASH).append(MULTIPART_BOUNDARY).append(HTTP_DOUBLE_DASH).append(HTTP_NEW_LINE);

        return b.toString();

    }

}