package br.com.neppo.jlibs.utils.data.reflect;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;

public class GetSetUtils {

    private GetSetUtils() {}

    @SuppressWarnings({ "unchecked" })
    public static <T> T get(Object o, String methodName, Class<T> rtnType){

        if( o == null || StringUtils.isEmpty(methodName) || rtnType == null){
            return null;
        }

        try{
            Method meth = o.getClass().getMethod(methodName);

            Object result = null;

            // breaking bad right here vvv
            if(meth != null){
                 meth.setAccessible(true);
                 result = meth.invoke(o);
            }

            if(result == null || !rtnType.isAssignableFrom(result.getClass())){
                return null;
            }
            return (T) result;

        }
        catch (Exception e){
            return null;
        }
    }

    public static <T extends Object> T set(T target, String methodName, Object value){

        if(target == null || StringUtils.isEmpty(methodName) || value == null){
            return null;
        }

        try{
            Method meth = target.getClass().getMethod(methodName, value.getClass());

            // breaking bad right here vvv
            if(meth != null){
                meth.setAccessible(true);
                meth.invoke(target, value);
            }

            return target;
        }
        catch (Exception e){
            return null;
        }
    }
}
