package br.com.neppo.jlibs.utils.flow.comparison;

import br.com.neppo.jlibs.utils.flow.comparison.interfaces.Current;
import br.com.neppo.jlibs.utils.flow.comparison.interfaces.PreviousCurrent;

import java.util.Collection;

public class ComparisonUtils {

    private ComparisonUtils() {}

    public static <Y> boolean applyComparison(PreviousCurrent comparer, Collection<Y> objs ){
        if(comparer == null || objs == null){
            return false;
        }

        Y previous = null;
        int index = 0;

        for(Y val : objs){
            if(index != 0 && !comparer.compare(previous, val)){
                return false;
            }
            previous = val;
            index ++;
        }
        return true;
    }

    public static <Y> boolean applySingleComparison(Current comparer, Collection<Y> objs ){
        if(comparer == null || objs == null){
            return false;
        }

        for(Y val : objs){
            if(!comparer.compare(val)){
                return false;
            }
        }
        return true;
    }
}
