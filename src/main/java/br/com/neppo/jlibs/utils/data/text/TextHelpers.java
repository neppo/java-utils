package br.com.neppo.jlibs.utils.data.text;

import org.apache.commons.lang3.StringUtils;

public class TextHelpers {
    private TextHelpers() { }

    public static String addForwardSlash(String input){
        if(StringUtils.isEmpty(input)){
            return "/";
        }
        return "/" + input;
    }

    public static String wrapQuote(String ... name) {
        if(name == null || name.length == 0){
            return "";
        }
        StringBuilder b = new StringBuilder();

        b.append('"');

        for (int i = 0; i < name.length; i++) {
            if(StringUtils.isEmpty(name[i])){
                continue;
            }
            b.append(name[i]);
        }

        b.append('"');

        return b.toString();
    }
}