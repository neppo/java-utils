package br.com.neppo.jlibs.utils.data.date;

import br.com.neppo.jlibs.utils.data.reflect.GetSetUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

    private DateUtils() {}

    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    public static boolean isBefore(Date date1, Date date2) {
        LocalDateTime startTime = dateToLocalDateTime(date1);
        LocalDateTime endingTime = dateToLocalDateTime(date2);
        return startTime.isBefore(endingTime);
    }

    public static Date getDateFromMinutesEarlier(int minutes) {
        return localDateTimeToDate(dateToLocalDateTime(new Date()).minusMinutes(minutes));
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static boolean isSame(Date a, Date b){
        if(a == b && a == null){
            return true;
        }
        if(a == null || b == null){
            return false;
        }
        return a.getTime() == b.getTime();
    }

    public static Date cast(String date, SimpleDateFormat format){
        if(StringUtils.isEmpty(date) || format == null || StringUtils.isEmpty(format.toPattern())){
            return null;
        }
        try{
            return format.parse(date);
        }
        catch (Exception e){
            return null;
        }
    }

    public static Date cast(String date, String format){
        if(StringUtils.isEmpty(format)){
            return null;
        }
        return cast(date, new SimpleDateFormat(format));
    }

    public static Date cast(Object o){
        if(o == null){
            return null;
        }

        if(o instanceof  Date){
            return (Date) o;
        }
        if(o instanceof Number){
            return new Date(Long.valueOf(String.valueOf(o)));
        }
        return null;
    }

    /**
     * will attempt fetch a date from informed getter
     * if field is a number, will attempt to cast it to a date (via epoch)
     * @param o object that holds the info
     * @param field getter field
     * @return a date
     */
    public static Date getAsDate(Object o, String field){
        if( o == null || StringUtils.isEmpty(field)){
            return null;
        }

        Object found = GetSetUtils.get(o, field, Object.class);

        return cast(found);
    }
}