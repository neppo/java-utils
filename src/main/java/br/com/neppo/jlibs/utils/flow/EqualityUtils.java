package br.com.neppo.jlibs.utils.flow;

public class EqualityUtils {

    private EqualityUtils(){ }

    public static boolean equals(Object o, Object o2){
        if(o == null && o2 == null){
            return true;
        }
        else if(o == null || o2 == null){
            return false;
        }

        return o.equals(o2);
    }

    public static <T> boolean typedEquals(T o, T o2){
        return equals(o, o2);
    }

}