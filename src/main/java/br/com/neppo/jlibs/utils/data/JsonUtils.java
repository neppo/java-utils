package br.com.neppo.jlibs.utils.data;

import br.com.neppo.jlibs.utils.dto.Rfc6902;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.flipkart.zjsonpatch.JsonPatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.flipkart.zjsonpatch.JsonDiff.asJson;
import static java.util.Objects.isNull;

public class JsonUtils {

    private JsonUtils() {
    }

    private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);
    private static ObjectMapper mapper = new ObjectMapper();

    public static String objectToJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T jsonToObject(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T tryConvertJsonToObject(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            logger.info("Failed to parse string to JSON");
            return null;
        }
    }

    public static Object convertStringToObject(String string) {
        Object result = tryConvertJsonToObject(string, Object.class);
        if (result == null) {
            result = string;
        }

        return result;
    }

    public static <T> List<T> jsonToList(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    public static <T> List<Rfc6902> patchDiff(T source, T target) {
        try {
            if (source != null && target != null) {
                JsonNode diff = asJson(JsonUtils.jsonNode(source), JsonUtils.jsonNode(target));
                List<Rfc6902> rfc6902s = jsonToList(JsonUtils.objectToJson(diff), Rfc6902.class);
                return rfc6902s;
            }
        } catch (RuntimeException e) {
            logger.error("Failed to get patch diff between objects", e);
        }
        return null;
    }

    public static <T> JsonNode jsonNode(T object) {
        try {
            return mapper.convertValue(object, JsonNode.class);
        } catch (RuntimeException e) {
            logger.error("Failed to parse complex object to JsonNode", e);
            return null;
        }
    }

    public static <T> T convertJsonNodeToObject(JsonNode jsonNode, Class<T> clazz) {
        try {
            return mapper.convertValue(jsonNode, clazz);
        } catch (RuntimeException e) {
            logger.error("Failed to parse JsonNode to complexObject", e);
            return null;
        }
    }

    public static <T> T applyPatch(T source, Collection<Rfc6902> toApply) {
        if (source != null
                && toApply != null && !toApply.isEmpty()) {
            Class<T> clazz = (Class<T>) source.getClass();

            toApply.forEach(apply -> {
                if (!isNull(apply.getValue())) {
                    Object valueObject = tryConvertJsonToObject(apply.getValue().toString(), Object.class);
                    if (valueObject != null) {
                        apply.setValue(valueObject);
                    }
                }
            });

            JsonNode applied = JsonPatch.apply(jsonNode(toApply), jsonNode(source));
            return convertJsonNodeToObject(applied, clazz);
        }
        return source;
    }
}