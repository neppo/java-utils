package br.com.neppo.jlibs.utils.net;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FileTransfer {

    boolean connect(String host, int port, String login, String password);

    void disconnect();

    List<String> listFilesInDir(String remoteDir) throws Exception;

    List<String> listSubDirInDir(String remoteDir) throws Exception;

    boolean createDirectory(String dirName);

    boolean downloadFile(String remotePath, String localPath) throws IOException;

    boolean downloadFileAfterCheck(String remotePath, String localPath) throws IOException;

    boolean uploadFile(String localPath, String remotePath) throws IOException;

    boolean uploadFile(InputStream inputFile, String remotePath) throws IOException;

    boolean changeDir(String remotePath) throws Exception;

    String getWorkingDirectory();

    InputStream downloadFile(String remotePath) throws Exception;

    void moveFilesBetween(String remoteOldpath, String remoteNewpath) throws Exception;

    boolean fileExists(String remotePath) throws IOException;
}
