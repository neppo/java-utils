package br.com.neppo.jlibs.utils.net.pojo;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class FormField {
    private String name;
    private byte[] value;
    private Map<String, String> headers = new LinkedHashMap<>();
    private String filename;

    public FormField(String name, String value) {
       this(name, value.getBytes());
    }

    public FormField(String name, byte[] value) {
        this.name = name;
        this.value = value;
    }

    public FormField addHeader(String name, String value){
        headers.put(name, value);
        return this;
    }

    public FormField fileName(String name){
        this.filename = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public byte[] getValue() {
        return value;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public boolean isFile() {
        return !StringUtils.isEmpty(this.filename);
    }

    public String getFilename() {
        return filename;
    }
}