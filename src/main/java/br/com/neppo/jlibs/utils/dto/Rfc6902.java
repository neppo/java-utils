package br.com.neppo.jlibs.utils.dto;

public class Rfc6902 {
    private String op;
    private String path;
    private Object value;
    private String from;
    private String fromValue;

    public Rfc6902(){
    }

    public Rfc6902(String op, String path, String value, String from, String fromValue) {
        this.op = op;
        this.path = path;
        this.value = value;
        this.from = from;
        this.fromValue = fromValue;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromValue() {
        return fromValue;
    }

    public void setFromValue(String fromValue) {
        this.fromValue = fromValue;
    }

    @Override
    public String toString() {
        return "Rfc6902{" +
                "op='" + op + '\'' +
                ", path='" + path + '\'' +
                ", value='" + value + '\'' +
                ", from='" + from + '\'' +
                ", fromValue='" + fromValue + '\'' +
                '}';
    }
}
